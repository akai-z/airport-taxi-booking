## Requirements
- PHP 7.4
- MariaDB 10.2+ (MySQL should work too. 
PostgreSQL should work as well since that the project uses an ORM but I have not tested it.)
- NGINX
- Node.js 12+
- Docker and Docker Compose (In case you want to use the prepared Docker images.)
- In case you want to use Docker, make sure that the ports are not blocked by a firewall, or taken by other processes.

## Setup Instructions (using Docker)
- Clone the repository using `Git`:
```
git clone git@bitbucket.org:akai-z/airport-taxi-booking.git
```
- Make sure that you are currently inside the cloned project directory.
```
cd <path-to-project-directory>/airport-taxi-booking
```
- Run the project Docker images with the following command:
```
docker-compose up -d --build
```
(MySQL might take some time to boot up after running its Docker image. So it might initially be not responsive.)

- Obtain Docker bridge network default gateway IP on your system (e.g. `172.17.0.1`).  
You could use the following command to find the details of the bridge network:
```
docker network inspect bridge
```
- Configure Symfony DB connection in Symfony `.env` file.  
For example:
```
DATABASE_URL="mysql://root:toor@172.17.0.1:3306/booking?serverVersion=mariadb-10.5.9"
```
(Where the IP `172.17.0.1` is the Docker bridge network gateway IP.)

- Configure Mailhog connection in Symfony `.env` file.  
For example:
```
MAILER_DSN=smtp://172.17.0.1:1025
```
(Where the IP `172.17.0.1` is the Docker bridge network gateway IP.)

- Access Docker PHP image with the following command:
```
docker-compose exec php sh
```
- Install Composer packages:
```
composer install
```
- Create the DB tables and add their data using the following commands:
```
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```
- Exit Docker PHP image with the `exit` command.
- Access Docker Node.js image with the following command:
```
docker-compose exec node sh
```
- Install Yarn packages:
```
yarn install --force
```
- Compile assets:
```
yarn encore dev
```
- Exit Docker Node.js image with the `exit` command.
- The application is now ready.

## Usage Instructions
- Open the following URL to access the taxi booking form page:
```
http://localhost:8080/booking
```
- You could check the emails sent by the form on Mailhog:
```
http://localhost:8025/
```
- You could view the submitted form requests details in phpMyAdmin:
```
http://localhost:8181/
```
(Username: `root` | Password: `toor`)  
Where the DB name is `booking`, and the name of the table that stores form requests is also `booking`.

## Notes about the project
- Since that I had to use Symfony forms with Vue.js on frontend side,  
I opted to build the form from scratch using Vue.js (HTML/JS) on frontend side and also build it  
using Symfony forms on the backend side.  
That way, I could leverage the full potential of Vue.js (features) on the frontend,  
(Instead of fetching the form HTML from Symfony forms using AJAX and use it on Vue.js)  
while at the same time being able to use Symfony forms on the backend to validate and submit forms data.

- I used `vue-datetime` component as a date picker on the form.  
There were issues with loading the component CSS in Symfony, possibly due to the webpack.  
As a temporary workaround I added the CSS file to the `public` directory and loaded it using a `twig` template.

- I wanted to implement some advanced validation features for `Date of Arrival` field, such as  
allowing dates that are in the future only and if a user already booked a taxi then he/she can only book another  
one after 3 days of the current booked taxi `Date of Arrival`.  
But due to some issues related to dates and timezones conversion on the server-side, I removed those server-side  
validation rules in order to not block form submit unexpectedly while testing the application.  
And I left it as something to be resolved in the future.  
`Vuelidate` library also had an issue related to validating dates that should always be in the future,  
where there is a trick to submit a form by waiting for the selected time to become in the past.  
So I skipped this frontend validation part as well, and left it as a TODO task.

- For Airflight number validation I used a format based on this [article](https://academe.co.uk/2014/01/validating-flight-codes/).  
It might be outdated or not accurate, but for this task, I think it should work as an example of airline code validation.

- For UK mobile number validation, I used a regular expression based on this [source](https://bit.ly/3wSaxBQ).  
However it might be outdated for the current UK mobile numbers, but it should work as an example of mobile validation for this task.

- I wanted to add a loading indicator to the form AJAX submit action,  
but there is almost no wait time when submitting the form to a local server.  
So load indicator is not necessary when the task will be tested on a local machine.
